//      ___           ___           ___       ___           ___     
//     /\  \         /\  \         /\__\     /\  \         |\__\    
//    /::\  \       /::\  \       /:/  /    /::\  \        |:|  |   
//   /:/\:\  \     /:/\:\  \     /:/  /    /:/\:\  \       |:|  |   
//  /::\~\:\  \   /::\~\:\  \   /:/  /    /::\~\:\  \      |:|__|__ 
// /:/\:\ \:\__\ /:/\:\ \:\__\ /:/__/    /:/\:\ \:\__\ ____/::::\__\
// \/_|::\/:/  / \:\~\:\ \/__/ \:\  \    \/__\:\/:/  / \::::/~~/~   
//    |:|::/  /   \:\ \:\__\    \:\  \        \::/  /   ~~|:|~~|    
//    |:|\/__/     \:\ \/__/     \:\  \       /:/  /      |:|  |    
//    |:|  |        \:\__\        \:\__\     /:/  /       |:|  |    
//     \|__|         \/__/         \/__/     \/__/         \|__|    

// A functional library, containing snippets. Usefull snippets! Not all own developed..

//     ___
//   _/ ..\
//  ( \  0/___
//   \    \___)
//   /     \
//  /      _\
// `''''``       
// filename: relax.lib.js
// created: 26/06/2014
// author: felix nielsen <felix@flexmotion.com>

// (function(a){function b(){}for(var c="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),d;!!(d=c.pop());){a[d]=a[d]||b;}})
// (function(){try{console.log();return window.console;}catch(a){return (window.console={});}}());

//Native functions
//----------------
if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length >>> 0;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
				 ? Math.ceil(from)
				 : Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}

// From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
if (!Object.keys) {
  Object.keys = (function () {
    'use strict';
    var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
        dontEnums = [
          'toString',
          'toLocaleString',
          'valueOf',
          'hasOwnProperty',
          'isPrototypeOf',
          'propertyIsEnumerable',
          'constructor'
        ],
        dontEnumsLength = dontEnums.length;

    return function (obj) {
      if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
        throw new TypeError('Object.keys called on non-object');
      }

      var result = [], prop, i;

      for (prop in obj) {
        if (hasOwnProperty.call(obj, prop)) {
          result.push(prop);
        }
      }

      if (hasDontEnumBug) {
        for (i = 0; i < dontEnumsLength; i++) {
          if (hasOwnProperty.call(obj, dontEnums[i])) {
            result.push(dontEnums[i]);
          }
        }
      }
      return result;
    };
  }());
}

//Native extensions
//-----------------
Date.prototype.getWeek = function (dowOffset) {
	dowOffset = typeof(dowOffset) == 'int' ? dowOffset : 1;
	var newYear = new Date(this.getFullYear(),0,1);
	var day = newYear.getDay() - dowOffset; //the day of week the year begins on
	day = (day >= 0 ? day : day + 7);
	var daynum = Math.floor((this.getTime() - newYear.getTime() - (this.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;
	var weeknum;
	// if the year starts before the middle of a week
	if(day < 4) {
		weeknum = Math.floor((daynum+day-1)/7) + 1;
		if(weeknum > 52) {
			nYear = new Date(this.getFullYear() + 1,0,1);
			nday = nYear.getDay() - dowOffset;
			nday = nday >= 0 ? nday : nday + 7;
			// if the next year starts before the middle of the week, it is week #1 of that year:
			weeknum = nday < 4 ? 1 : 53;
		}
	} else {
		weeknum = Math.floor((daynum+day-1)/7);
	}
	return weeknum;
};

/* Array sorting algoritm */
//how to use: sorting
// var sorted = [
//   'Finger',
//   'Sandwich',
//   'sandwich',
//   '5 pork rinds',
//   'a guy named Steve',
//   'some noodles',
//   'mops and brooms',
//   'Potato Chip Brand® chips'
// ].msort(function(left, right)
// {
//   lval = left.toLowerCase();
//   rval = right.toLowerCase();

//   if (lval < rval)
//     return -1;
//   else if (lval == rval)
//     return 0;
//   else
//     return 1;
// });

// console.log(sorted);
Array.prototype.msort = msort;

function msort(compare) {
	var length = this.length,
	    middle = Math.floor(length / 2);

	if (!compare) {
	  compare = function(left, right) {
	    if (left < right)
	      return -1;
	    if (left == right)
	      return 0;
	    else
	      return 1;
	  };
	}

	if (length < 2)
	  return this;

	return merge(
	  this.slice(0, middle).msort(compare),
	  this.slice(middle, length).msort(compare),
	  compare
	);
}

function merge(left, right, compare) {
	var result = [];

	while (left.length > 0 || right.length > 0) {
	  if (left.length > 0 && right.length > 0) {
	    if (compare(left[0], right[0]) <= 0) {
	      result.push(left[0]);
	      left = left.slice(1);
	    }
	    else {
	      result.push(right[0]);
	      right = right.slice(1);
	    }
	  }
	  else if (left.length > 0) {
	    result.push(left[0]);
	    left = left.slice(1);
	  }
	  else if (right.length > 0) {
	    result.push(right[0]);
	    right = right.slice(1);
	  }
	}
	return result;
}

//---------------
(function()
{
	try
	{
		/* jQuery widgets - author felix nielsen */
		/* delete duplicate dom elements, right now it checks for text.. should add other posibilities. */
		jQuery.fn.msort = msort;
	
		jQuery.fn.deleteDuplicates = function()
		{
			var seen = {};
			this.each(function()
			{
				var txt = String($j(this).text()).toLowerCase();
				if (seen[txt])
					$j(this).remove();
				else
					seen[txt] = true;
			});

			return this;
		};
	}catch(err)
	{
		//jQuery is not defined.
	}
})();


























var relax = {};
relax.animation = {}; // animation related relaxs.
relax.browser = {}; // browser related relaxs.
relax.math = {};
relax.geom = {};
relax.color = {};
relax.performance = {};
relax.layout = {};
relax.caniuse = {};
relax.tools = {};
relax.displayObject = {};

/* Layout helpers */
relax.layout.DeviceOrientChange = function(callback)
{
	var mode = Math.abs(window.orientation) === 90 ? "landscape" : "portrait";
	//singleton, don't make than one instance of this.
	//callback get's parameters mode and angle
	function readDeviceOrientation() {
		mode = Math.abs(window.orientation) === 90 ? "landscape" : "portrait";
		callback(mode, window.orientation);
	}
	callback(mode, window.orientation);

	window.onorientationchange = readDeviceOrientation;
};

/**
* scales a DOM element to a rect.
* 
* @param	rect:Object 	an object created with relax.layout.getScaleToRect
* @param	el 				can be a jquery element or just plain old vanilla js element ex.: document.getElementById("myDiv")
*/
relax.layout.scaleToRect = function(rect, el)
{
	if(jQuery)
	{
		$(el).css({
			"width": rect.w,
			"height": rect.h,
			"margin-left": rect.overflowx,
			"margin-top": rect.overflowy
		});
	}else
	{
		el.style.width = rect.w;
		el.style.height = ect.h;
		el.style.marginLeft = rect.overflowx;
		el.style.marginTop = rect.overflowy;
	}
};

/**
* get a rectangle object that is scaled within a defined box. Keeping original aspect ratios in mind.
* 
* @param	origw: Number 			the original widht of the box
* @param	origh: Number 			the original height of the box
* @param	w: Number 				the width of the new box
* @param	w: Number 				the height of the new box
* @param	ignoreHeight: Boolean 	to scale on width only or not.
* @return 	Object
*/
relax.layout.getScaleToRect = function(origw, origh, w, h, ignoreHeight)
{
	var ww = w;
	var hh = (ww / origw) * origh;

	if(hh < h && !ignoreHeight)
		hh = h;
		ww = (hh / origh) * origw;

	var overflowx = w * 0.5 - ww * 0.5;
	var overflowy = ignoreHeight ? 0 : h * 0.5 - hh * 0.5;

	return {
		w: ww,
		h: hh,
		overflowx: overflowx,
		overflowy: overflowy
	};
};

/* Geometry functions */

/**
* get the distance in pixels between two points.
* the points is 2d vectors, so Objects with x and y values.
* 
* @param	point1:Vector	an Object with x, y
* @param	point2:Vector	an Object with x, y
* @return	distance between the two vectors.
*/
relax.geom.lineDistance = function( point1, point2 )
{
	var xs = 0;
	var ys = 0;

	xs = point2.x - point1.x;
	xs = xs * xs;

	ys = point2.y - point1.y;
	ys = ys * ys;

	return Math.sqrt( xs + ys );
};

/**
* Converts a number in the degree spectrum to the equvalent number in the radians spectrum.
* 
* @param	radians:Number	a radians number
* @return	Number
*/
relax.geom.degToRad = function(degree)
{
	return degree / 180 * Math.PI;
};

/**
* Converts a number in the radians spectrum to the equvalent number in the degrees spectrum.
* 
* @param	radians:Number	a radians number
* @return	Number
*/
relax.geom.radToDeg = function(radians)
{
	return radians * 180 / Math.PI
};

/**
* Polyfill for getting the x and y values of a mouse/touch device.
* 
* @param	event 	Javascript event object.
* @return	object
				x: Number
				y: Number
				touches: Array
*/
relax.geom.getXYFromMouseTouchEvent = function(event)
{
	if(event.originalEvent && (event.originalEvent.touches || event.originalEvent.changedTouches))
	{
		var touch = event.originalEvent.touches[0] ? event.originalEvent.touches : event.originalEvent.changedTouches;
		return {x: touch[0].pageX, y: touch[0].pageY, touches: touch};
	}
	else
	{
		return {x: event.pageX, y: event.pageY, touches: null};
	}
};


/* Math functions */

/**
* Calculates a number between two numbers at a specific increment, and returns a percentage representation.
* 
* @param	value:Number	number between the min, max range.
* @param	min:Number		min value
* @param	max:Number		max value
* @return	Number
*/
//relax.math.norm(50, 0, 100) -> 0.5
relax.math.norm = function(value, min, max)
{
	return (value - min) / (max - min);
};

/**
* Calculates a number between two numbers at a specific increment.
* 
* @param	value:Number	percentage value (0 - 1)
* @param	min:Number		min value
* @param	max:Number		max value
* @return	Number
*/
//relax.math.lerp(0.50, 0, 100) -> 50
relax.math.lerp = function(norm, min, max)
{
	return (max - min) * norm + min;
};

/**
* lerps a value in its source range, then normalizes the equvalent value of the destination range.
* 
* @param	value:Number		value to consider
* @param	sourceMin:Number	source min value
* @param	sourceMax:Number	source max value
* @param	destMin:Number		destination min value
* @param	destMax:Number		destination max value
* @return	Number
*/
//relax.math.map(50, 0, 100, 0, 50) -> 25
relax.math.map = function(value, sourceMin, sourceMax, destMin, destMax)
{	
	return relax.math.lerp(relax.math.norm(value, sourceMin, sourceMax), destMin, destMax);
};

/**
* Makes sure a number stays within range.
* 
* @param	value:Number	value to consider
* @param	min:Number		min number for range
* @param	max:Number		max number for range
* @return	Number
*/
//relax.math.clamp(50, 10, 40) -> 40
relax.math.clamp = function(value, min, max)
{	
	return Math.min(Math.max(value, min), max);
};

/**
* Makes sure a number stays within range.
* 
* @param	value:Number		value to consider in srcRange
* @param	srcRange:Array		the original range
* @param	dstRange:Array		the destination range.
* @return	Number 				returns range number of the dstRange.
*/
//convertToRange(20,[10,50],[5,10]); -> 6.25
relax.math.convertToRange = function(value, srcRange, dstRange) {
	if (value < srcRange[0]){
		return dstRange[0];
	} else if (value > srcRange[1]){
		return dstRange[1];
	} else {
		var srcMax = srcRange[1] - srcRange[0],
			dstMax = dstRange[1] - dstRange[0],
			adjValue = value - srcRange[0];

		return (adjValue * dstMax / srcMax) + dstRange[0];
	}	
};








/* color tools */

/**
* Converts red, green, blue, alpha values to 24bit Hexidecimal String, ready for css
* 
* @param	r:Number		Number representing the red channel of RGB(0-255)
* @param	g:Number		Number representing the green channel of RGB(0-255)
* @param	b:Number		Number representing the blue channel of RGB(0-255)
* @param	alpha:Number	Number representing the alpha channel of RGB(0-255)
* @param	trueHex:Boolean	if we need to return a hex value with # or pure Hex 24bit value.
* @return	Object			A string value or 24bit hex
*/
// relax.color.rgb2hex(255, 255, 255, 1, true) -> 33554431
// relax.color.rgb2hex(255, 255, 255, 0, true) -> -1
// relax.color.rgb2hex(255, 255, 255, 0.5, true) -> 16777215
// relax.color.rgb2hex(255, 255, 255, 0.5) -> "#ffffff"
relax.color.rgb2hex = function(r, g, b, a, trueHex)
{
	//helper function
	function componentToHex(c) {
	    var hex = c.toString(16);
	    return hex.length == 1 ? "0" + hex : hex;
	}

	if(!a)
		a = 255;//default value

	if(!trueHex)
	{
		if(a <= 1)//decimal..
			a = Math.round(a * 255);

		return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
	}else
		return (a << 24) | (r << 16) | (g << 8) | b;
};


/**
* Converts a 24bit Hexidecimal to a red, green, blue Object
* 
* @param	hex		A 24bit Hexidecimal to convert
* @return	Object	An Object containign values for red, green and blue
*/
// relax.color.hex2rgb(0x00ff00) -> Object {r: 0, g: 255, b: 0}
relax.color.hex2rgb = function(color)
{
	var c = {};

	c.red = color >> 16 & 0xFF;
	c.green = color >> 8 & 0xFF;
	c.blue = color & 0xFF;

	return c;
};


/**
* Reduces the input BitmapData's colour palette
* 
* @param	color	ex.: 0xffff00
* @return	String 	hex value with #
*/
// relax.color.hex2css(0xffff00) -> "#ffff00"
relax.color.hex2css = function(color)
{
	return "#" + color.toString(16);
};



/**
* Reduces the input BitmapData's colour palette
* 
* @param	percent		0 - 1
* @param	highColor	color 1
* @param	lowColor	color 2
* @return	String 		hex value with #
*/
// relax.color.getBetweenColourByPercent(0.5, 0xffffff, 0x000000) -> 8355711
relax.color.getBetweenColourByPercent = function(percent, highColor, lowColor)
{
	var r = highColor >> 16;
	var g = highColor >> 8 & 0xFF;
	var b = highColor & 0xFF;

	r += ((lowColor >> 16) - r) * percent;
	g += ((lowColor >> 8 & 0xFF) - g) * percent;
	b += ((lowColor & 0xFF) - b) * percent;

	var hex = (r << 16 | g << 8 | b);
	return relax.color.hex2css(hex);
};



/**
* Calculates whether colourA (hex) and colourB (hex) are similar within a given tolerance
* 
* @param	colour1		The first colour (hex)
* @param	colour2		The second colour (hex)
* @param	tolerance	The tolerance of the algorythm
* @return	Boolean		Whether the given colours a similar
*/

// relax.color.similar(0xff00ff, 0x000000, 0) -> false
// relax.color.similar(0xff00ff, 0x000000, 0.5) -> false
// relax.color.similar(0xff00ff, 0x000000, 0.1) -> false
// relax.color.similar(0xff00ff, 0x000000, 0.9) -> true

relax.color.similar = function(colour1, colour2, tolerance)
{
	if(!tolerance)
		tolerance = 0.01;

	var RGB1 = relax.color.hex2rgb( colour1 );
	var RGB2 = relax.color.hex2rgb( colour2 );

	tolerance = tolerance * ( 255 * 255 * 3 ) << 0;

	var distance = 0;

	distance += Math.pow( RGB1.red - RGB2.red, 2 );
	distance += Math.pow( RGB1.green - RGB2.green, 2 );
	distance += Math.pow( RGB1.blue - RGB2.blue, 2 );

	return distance <= tolerance;
};


/**
* Compares a given colour to a set of colours an evaluates whether or not
* the colour is sufficiently unique
* 
* @param	colour		The colour to analyse
* @param	colours		An array of colours to compare the colour to
* @param	tolerance	The tolerance of the algorythm
* @return	Boolean		Whether the given colour is sufficiently unique
*/

relax.color.different = function(colour, colours, tolerance)
{
	if(!tolerance)
		tolerance = 0.01;

	for (var i = 0; i < colours.length; i++) 
	{
		if (relax.color.similar(colour, colours[i], tolerance))
		{
			return false;
		}
	}
	return true;
};



/**
* Returns an array of unique colours up to a given maximum
* 
* @param	colours		The colours to compare
* @param	maximum		The maximum amount of colours to return
* @param	tolerance	The tolerance of the algorythm
* @return	Array		An array of unique colours
*/

// relax.color.uniqueColours([0xff00ff, 0xffffff, 0x322432], 2) -> [16711935, 16777215]

relax.color.uniqueColours = function(colours, maximum, tolerance)
{
	if(!tolerance)
		tolerance = 0.01;

	var unique = [];

	for (var i = 0; i < colours.length && unique.length < maximum; i++) 
	{
		if (relax.color.different( colours[i], unique, tolerance ))
		{
			unique.push(colours[i]);
		}
	}

	return unique;
};





/**
* Generates an array of objects representing each colour present
* in an image. Each object has a 'colour' and a 'count' property
* 
* @param	source		The canvas to index, ex. document.getElementById("canvas")
* @param	sort		Whether to sort results by their count
* @return	Array		An array of { colour:int, count:int } objects
*/

relax.color.indexColours = function(canvas, sort) // : Array
{
	if(!sort)
		sort = true;

	var ctx = canvas.getContext("2d")

	var imgd = ctx.getImageData(0, 0, canvas.width, canvas.height);
	var pix = imgd.data;

	var n = {};
	var a = [];
	var p;

	var hex;
	for (var i = 0, n = pix.length; i < n; i += 4)
	{
		hex = relax.color.rgb2hex(pix[i], pix[i + 1], pix[i + 2])
		a.push({
			colour: hex,
			count: a.length
		})
	}

	if ( !sort ) return a;

	function byCount( a, b )// : int
	{
		if ( a.count > b.count ) return 1;
		if ( a.count < b.count ) return - 1;
		return 0;
	}

	return a.sort(byCount);
};



/**
* Calculates the average colour in a BitmapData Object
* 
* @param	canvas		The BitmapData Object to analyse
* @return	uint		The average colour in the BitmapData

* This uses the RGB color spectrum, which is not the best way of getting the average color from a picture.
* Read here for the "perfect" way: http://stackoverflow.com/a/2542430
*/
relax.color.averageColour = function(canvas) // : uint
{
	var r = 0;
	var g = 0;
	var b = 0;
	var n = 0;
	var hex;

	var ctx = canvas.getContext("2d");
	var imgd = ctx.getImageData(0, 0, canvas.width, canvas.height);
	var pix = imgd.data;

	for (var i = 0, m = pix.length; i < m; i += 4)
	{
		r += pix[i];
		g += pix[i + 1];
		b += pix[i + 2];
		n++
	}

	//flooring the values
	r = ~~(r/n);
    g = ~~(g/n);
    b = ~~(b/n);

	return relax.color.rgb2hex(r, g, b, 1, false);
};


/**
* Extracts the average colours from an image by dividing the canvas
* into segments and then finding the average colour in each segment
* 
* @param	canvas		The canvas image to analyse
* @param	colours		The amount of colours to extract
* @return	Array		An array of averaged colours
*/

relax.color.averageColours = function( canvas, colours ) // : Array
{
	var averages = new Array();
	var columns = Math.round( Math.sqrt( colours ) );
	var ctx = canvas.getContext("2d");

	var row = 0;
	var col = 0;
	var x = 0;
	var y = 0;
	var c;
	var localctx;
	var localdata;

	var w = Math.round( canvas.width / columns );
	var h = Math.round( canvas.height / columns );
	
	for (var i = 0; i < colours; i++) 
	{
		c = document.createElement('canvas');
		localctx = c.getContext("2d");
		localdata = ctx.getImageData(x,y,w,h);
		localctx.putImageData(localdata,10,70);

		averages.push(relax.color.averageColour(c));

		col = i % columns;

		x = w * col;
		y = h * row;

		if ( col == columns - 1 ) row++;
	}

	return averages;
};


/**
* Reduces the input BitmapData's colour palette
* 
* @param	source	The BitmapData to change
* @param	colours	The number of colours to reduce the BitmapData to
*/

// public static function reduceColours( source:BitmapData, colours:int = 16 ):void
// {
// 	var Ra:Array = new Array(256);
// 	var Ga:Array = new Array(256);
// 	var Ba:Array = new Array(256);

// 	var n:Number = 256 / ( colours / 3 );
// 	for (var i:int = 0; i < 256; i++)
// 	{
// 		Ba[i] = Math.floor(i / n) * n;
// 		Ga[i] = Ba[i] << 8;
// 		Ra[i] = Ga[i] << 8;
// 	}
// 	source.paletteMap( source, source.rect, new Point(), Ra, Ga, Ba );
// }
















/*performance*/
relax.performance.testStart = function()
{
	this.start = (new Date()).getTime();
};

relax.performance.testEnd = function()
{
	if(!this.start)
		log ("did you call start yet?");
	return (new Date()).getTime() - this.start;
};


/* Globals */

/* can i use methods */
relax.caniuse.pushstate = function() { return !!(window.history && history.pushState); };

relax.caniuse.canvas =  function(){
	if(this._canvasSupported == undefined || this._canvasSupported == null)
	{
		var elem = document.createElement('canvas');
		this._canvasSupported = !!(elem.getContext && elem.getContext('2d'));
	}
	
	return this._canvasSupported;
};

// can you use the mp4 format
// from: http://stackoverflow.com/a/8134378
relax.caniuse.mp4 = function()
{
	if(this.mp4Supported) return this.mp4Supported
	
	this.mp4Supported = (!!document.createElement('video').canPlayType('video/mp4; codecs=avc1.42E01E,mp4a.40.2'));

	return this.mp4Supported
}

/* whois browser */
relax.browser.isIOS = function()
{
	if(navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i))
		return true;
	else
		return false;
};

relax.browser.getIOSVersion = function() {
	if(!relax.isIOS())
		return -1;

	var agent = window.navigator.userAgent,
	start = agent.indexOf( 'OS ' );

	if( ( agent.indexOf( 'iPhone' ) > -1 || agent.indexOf( 'iPad' ) > -1 || agent.indexOf( 'iPod' ) > -1 ) && start > -1 ){
		return window.Number( agent.substr( start + 3, 3 ).replace( '_', '.' ) );
	} else {
		return 0;
	}
};

relax.browser.isAndroid = function()
{
	return navigator.userAgent.match(/Android/i);
};

relax.browser.isWindowsPhone = function()
{
	if(navigator.userAgent.match(/Windows Phone/i) || navigator.userAgent.match(/IEMobile/i))
		return true;
	else
		return false;
};

relax.browser.isTablet = function()
{
	//Android mobiles have Mobile in the user agent, this is not a 100% solution..
	var isAndroidTablet = relax.browser.isAndroid() && !navigator.userAgent.match(/Mobile/i) && !navigator.userAgent.match(/mobile/i);

	if(navigator.userAgent.match(/iPad/i) || isAndroidTablet)
		return true;
	else
		return false;
};

relax.browser.isMobile = function()
{
	//Android mobiles have Mobile in the user agent, this is not a 100% solution..
	var isMobile = !relax.browser.isTablet() && (navigator.userAgent.match(/Mobile/i) || navigator.userAgent.match(/mobile/i));
	var isiOS = !relax.browser.isTablet() && relax.browser.isIOS();
	var isWindows = relax.browser.isWindowsPhone();

	if(isMobile || isiOS || isWindows)
		return true;
	else
		return false;
};

relax.browser.getInternetExplorerVersion = function()
{
	var rv = -1; // Return value assumes failure.
	if(navigator.appName == 'Microsoft Internet Explorer')
	{
		var ua = navigator.userAgent;
		var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");

		if (re.exec(ua) != null)
			rv = parseFloat(RegExp.$1);
	}

	return rv;
};

/* Tools */
relax.cookie = {
	get: function(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name)
			{
				return unescape(y);
			}
		}
	},
	set: function(c_name, value, exdays)
	{
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays===null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	},
	clear: function(c_name)
	{
		document.cookie = c_name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	},
	check: function(c_name, exdays)
	{
		var label=this.get(c_name);
		if (label && label !== "" && label !== undefined && label !== null && label !== "undefined")
		{
			//exists.
			return true;
		}

		return false;
	}
};

relax.getTransformProperty = function(element) {
	// Note that in some versions of IE9 it is critical that
	// msTransform appear in this list before MozTransform
	var properties = [
			'transform',
			'WebkitTransform',
			'msTransform',
			'MozTransform',
			'OTransform'
	];

	var p;
	while (p = properties.shift()) {
		if (typeof element.style[p] != 'undefined') {
				return p;
		}
	}
	return false;
};

/*throttle method.
Use with jquery: $('...').keypress(relax.throttle(function (event) { ... }, 250, this));
Or as standalone: relax.throttle(this.Method, 1000, this)(arguments);
*/
relax.throttle = function(fn, delay, context)
{
	var timer = null;
	if(!context) context = this;
	return function () {
		var args = arguments;
		clearTimeout(timer);
		timer = setTimeout(function () {
			if(context) fn.apply(context, args);
		}, delay);
	};
};

/* cross browser way to listen for an option (in a select element) change */
relax.bindSelect = function(select, scope, callback)
{
	var ie8AndDown = relax.browser.getInternetExplorerVersion() <= 8 && relax.browser.getInternetExplorerVersion() !== -1;
	var _callback = callback, _scope = scope;

	//bind events.
	if(!ie8AndDown)
	{
		$(select).change(relax.bind(scope, _callback));
	}
	else
	{
		$(select).bind('propertychange', function(e) {
			if(e.type == "propertychange" && event.propertyName.toLowerCase() == "value")
			{
				e.target.selectedIndex = this.selectedIndex;
				_callback(e);
			}
		});
	}
};

/*Usage (from modinizr):
element.addEventListener(relax.transitionEndString, theFunctionToInvoke, false);
*/
relax.transitionEndString = 'transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd';

relax.bind = function(scope, fn) {
	return function () {
		fn.apply(scope, arguments);
	};
};

relax.getRandomNumber = function(start, range, roundUp)
{
	var ran = start + (roundUp ? Math.round(Math.random() * (range - start)) : Math.random() * (range - start));
	return ran;
};

relax.validateEmail = function(emailToValidate)
{
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(emailToValidate);
};

relax.validatePassword = function(password)
{
	var re = /^[A-Za-z0-9!@#$%^&*()_]{1,50}$/;
	return re.test(password);
};

relax.stripHTMLFromString = function(str)
{
	var tmp = document.createElement("DIV");
	tmp.innerHTML = str;
	return tmp.textContent||tmp.innerText;
};











/*Interface tool for Class implementation below*/
relax.checkInterface = function(theObject, theInterface) {
	for (var member in theInterface) {
		if ( (typeof theObject[member] != typeof theInterface[member]) ) {
			log("object failed to implement interface member " + member);
			return false;
		}
	}

	return true;
};
/* External tools */


/* (framework) Class inheritance made easy */
/* Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 */
(function(){
	var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;
	// The base Class implementation (does nothing)
	this.Class = function(){};
	
	// Create a new Class that inherits from this class
	Class.extend = function(prop) {
		var _super = this.prototype;
		
		// Instantiate a base class (but only create the instance,
		// don't run the init constructor)
		initializing = true;
		var prototype = new this();
		initializing = false;
		
		// Copy the properties over onto the new prototype
		for (var name in prop) {
			// Check if we're overwriting an existing function
			prototype[name] = typeof prop[name] == "function" &&
				typeof _super[name] == "function" && fnTest.test(prop[name]) ?
				(function(name, fn){
					return function() {
						var tmp = this._super;
						
						// Add a new ._super() method that is the same method
						// but on the super-class
						this._super = _super[name];
						
						// The method only need to be bound temporarily, so we
						// remove it when we're done executing
						var ret = fn.apply(this, arguments);
						this._super = tmp;
						
						return ret;
					};
				})(name, prop[name]) :
				prop[name];
		}
		
		// The dummy class constructor
		function Class() {
			// All construction is actually done in the init method
			if ( !initializing && this.init )
				this.init.apply(this, arguments);
		}
		
		// Populate our constructed prototype object
		Class.prototype = prototype;
		
		// Enforce the constructor to be what we expect
		Class.prototype.constructor = Class;

		// And make this class extendable
		Class.extend = arguments.callee;
		
		return Class;
	};

	if(typeof define === 'function' && define.amd)
	{
		define("Class", function()
		{
			return Class;
		});
	}

})();














/* Display objects */
relax.displayObject.statsWrapper = {
	stats: null,
	init: function()
	{
		//add stats
		this.stats = new Stats();

		// Align top-left
		this.stats.getDomElement().style.position = 'fixed';
		this.stats.getDomElement().style.left = '0px';
		this.stats.getDomElement().style.top = '0px';

		document.body.appendChild( this.stats.getDomElement() );

		RenderQue.add(this);
	},
	renderQueCall: function()
	{
		this.stats.update();
	}
};

/* Extended Image object
* usage 1:
* img = new relax.displayObject.Image(relax.bind(@, @onImageLoaded), src)
*
* usage 2:
* img = new relax.displayObject.Image(relax.bind(@, @onImageLoaded))
* //now get progress in a load object: img.getProgress()
* img.load(src)
*/
relax.displayObject.Image = Class.extend({
	_callback: null,
	_progress: 0,
	data: null,
	loaded: false,
	naturalWidth: 0,
	naturalHeight: 0,
	init: function(callback, src)
	{
		this._callback = callback;

		//this.data = new Image();
		//this.data.onload = relax.bind(this, this.onImageLoaded);
		//this.data.src = src;

		if(src)
		{
			//if src is set, then load the image as normal
			this.load(src);
		}
	},
	loadWithoutXHR: function(src)
	{
		this.data = new Image();
		this.data.onload = relax.bind(this, this.onImageLoaded);
		this.data.src = src;
	},
	/*
	* src: string
	* preventXHR (optional): make it possible to prevent using XMLHttpRequest
	*/
	load: function(src, preventXHR)
	{
		if(preventXHR || relax.browser.getInternetExplorerVersion() !== -1)
		{
			//internet explorer is not a fan of cross domain loading, so just to be safe, we load normally.
			this.loadWithoutXHR(src);
			this._progress = 1;//set progress to 1 to give a fake sense of loading :)
		}else
		{
			this.request = new XMLHttpRequest();
			//this.request.onloadstart = relax.bind(this, this.onImageLoaded);
			this.request.onprogress = relax.bind(this, this.onLoadProgress);
			this.request.onload = relax.bind(this, this.onRequestLoaded);
			//this.request.onloadend = ..;
			this.request.open("GET", src, true);
			this.request.overrideMimeType('text/plain; charset=x-user-defined'); 
			this.request.send(null);
		}
	},
	onRequestLoaded: function()
	{
		this.data = new Image();
		this.data.onload = relax.bind(this, this.onImageLoaded);
		this.data.src = "data:image/jpeg;base64," + this.base64Encode(this.request.responseText);
	},
	onLoadProgress: function(event)
	{
		if (event.lengthComputable)
			this._progress = event.loaded / event.total;
	},
	stop: function()
	{	
		if(this.data)
			this.data.src = "";

		if(this.request)
		{
			this.request.onprogress = null;
			this.request.onload = null;
			this.request.abort();
		}
	},
	getProgress: function()
	{
		return this._progress;
	},
	dealoc: function()
	{
		this.stop()
		if(this.data)
		{
			this.data.onload = undefined;
			this.data = null;
		}

		this._callback = null;

		this.request = null
	},
	onImageLoaded: function()
	{
		this._progress = 1;
		this.naturalWidth = this.data.naturalWidth
		this.naturalHeight = this.data.naturalHeight

		this.loaded = true;
		this._callback(this);
	},
	base64Encode: function(inputStr) 
	{
		var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
		var outputStr = "";
		var i = 0;

		while (i < inputStr.length)
		{
			//all three "& 0xff" added below are there to fix a known bug 
			//with bytes returned by xhr.responseText
			var byte1 = inputStr.charCodeAt(i++) & 0xff;
			var byte2 = inputStr.charCodeAt(i++) & 0xff;
			var byte3 = inputStr.charCodeAt(i++) & 0xff;

			var enc1 = byte1 >> 2;
			var enc2 = ((byte1 & 3) << 4) | (byte2 >> 4);

			var enc3, enc4;
			if (isNaN(byte2))
			{
				enc3 = enc4 = 64;
			}
			else
			{
				enc3 = ((byte2 & 15) << 2) | (byte3 >> 6);
				if (isNaN(byte3))
				{
					enc4 = 64;
				}
				else
				{
				enc4 = byte3 & 63;
				}
			}

			outputStr += b64.charAt(enc1) + b64.charAt(enc2) + b64.charAt(enc3) + b64.charAt(enc4);
		} 

		return outputStr;
	}
});


/* TOOLS */

/* Drag and swipe gesture */
relax.tools.DragAndSwipe = Class.extend({
	_el: null,
	_touchEventsAdded: false,
	_pinchCallback: null,
	_swipeCallback: null,
	_dragCallback: null,

	_swipeThreshold: 20,

	_startY: 0,
	_dragY: 0,
	_dragYTarget: 0,
	_startX: 0,
	_dragX: 0,
	_dragXTarget: 0,

	_maxMinBoundries: null,

	init: function(el)
	{
		this._el = el;
	},
	setBoundries: function(obj)
	{
		// all optional

		// obj:
		// 	minSwipeY: Math.max(minSwipeY, y)
		// 	maxSwipeY: Math.min(100, y)
		// 	minSwipeX: Math.max(minSwipeX, x)
		// 	maxSwipeX: Math.min(100, x)

		this._maxMinBoundries = obj;
	},
	_onTouchStart: function(event)
	{
		//event.preventDefault();
		obj = relax.geom.getXYFromMouseTouchEvent(event);

		this._startY = obj.y;
		this._startX = obj.x;

		this._onDragging();
	},
	_onTouchEnd: function(event)
	{
		//event.preventDefault();
		obj = relax.geom.getXYFromMouseTouchEvent(event);

		var anchorY = (obj.y - this._startY);
		var anchorX = (obj.x - this._startX);
		this._dragY = this._dragY + anchorY;
		this._dragX = this._dragX + anchorX;

		if(this._swipeCallback)
		{
			if(this._swipeCallback && Math.abs(this._endX - this._startX) > 100)
			{
				this._swipeCallback({
					xDirection: this._endX > this._startX ? "left" : "right",
					yDirection: this._endY > this._startY ? "up" : "down"
				});
			}
		}

		this._onDragging();
	},
	_onTouchMove: function(event)
	{
		event.preventDefault();

		obj = relax.geom.getXYFromMouseTouchEvent(event);

		var anchorY = (obj.y - this._startY);
		var anchorX = (obj.x - this._startX);
		this._dragYTarget = this._dragY + anchorY;
		this._dragXTarget = this._dragX + anchorX;

		this._onDragging();
	},
	_onDragging: function()
	{
		if(this._maxMinBoundries)
		{
			if(this._maxMinBoundries.minSwipeX)
			{
				this._dragXTarget = Math.max(this._maxMinBoundries.minSwipeX, this._dragXTarget);
				this._dragX = Math.max(this._maxMinBoundries.minSwipeX, this._dragX);
			}
			if(this._maxMinBoundries.minSwipeY)
			{
				this._dragYTarget = Math.max(this._maxMinBoundries.minSwipeY, this._dragYTarget);
				this._dragY = Math.max(this._maxMinBoundries.minSwipeY, this._dragY);
			}
			if(this._maxMinBoundries.maxSwipeX)
			{
				this._dragXTarget = Math.min(this._maxMinBoundries.maxSwipeX, this._dragXTarget);
				this._dragX = Math.min(this._maxMinBoundries.maxSwipeX, this._dragX);
			}
			if(this._maxMinBoundries.maxSwipeY)
			{
				this._dragYTarget = Math.min(this._maxMinBoundries.maxSwipeY, this._dragYTarget);
				this._dragY = Math.min(this._maxMinBoundries.maxSwipeY, this._dragY);
			}
		}

		if(this._dragCallback)
		{
			this._dragCallback({
				x: this._dragXTarget,
				y: this._dragYTarget
			});
		}
	},
	addTouchEvents: function()
	{
		if(this._touchEventsAdded) return;

		this._touchEventsAdded = true;

		$(this._el).bind("touchstart.relax", relax.bind(this, this._onTouchStart));
		$(this._el).bind("touchmove.relax", relax.bind(this, this._onTouchMove));
		$(this._el).bind("touchend.relax", relax.bind(this, this._onTouchEnd));
	},
	// swipeThreshold in pixels
	addSwipe: function(swipeCallback, swipeThreshold)
	{
		if(swipeThreshold) this._swipeThreshold = swipeThreshold

		this._swipeCallback = swipeCallback;
		this.addTouchEvents()
	},
	addDrag: function(dragCallback)
	{
		this._dragCallback = dragCallback;
		this.addTouchEvents()
	},
	dealoc: function()
	{
		this._binded = false;
		$(this._el).unbind("touchstart.relax touchmove.relax touchend.relax");
		this._el = null;
		this._callback = null;
	}
});

/* Pinch gesture */

var PinchGestureWrapper = Class.extend({
	_el: null,
	_callback: null,
	_startScale: 0,
	_currentScale: 0,

	init: function(el, callback)
	{
		this._el = el;
		this._callback = callback;
		this._startScale = 0;
		this._currentScale = 0;

		this._onGestureStart = function(event)
		{
			event.preventDefault();
			this._startScale = event.originalEvent.scale;
		};

		this._onGestureEnd = function(event)
		{
			event.preventDefault();
			this._callback(this._startScale > this._currentScale);
		};

		this._onGestureChange = function(event)
		{
			event.preventDefault();
			this._currentScale = event.originalEvent.scale;
		};

		$(this._el).bind("gesturestart.pinch", relax.bind(this, this._onGestureStart));
		$(this._el).bind("gesturechange.pinch", relax.bind(this, this._onGestureChange));
		$(this._el).bind("gestureend.pinch", relax.bind(this, this._onGestureEnd));
	},
	dealoc: function()
	{
		$(this._el).unbind("gesturestart.pinch gesturechange.pinch gestureend.pinch");
		this._el = null;
		this._callback = null;
	}
});

/* render cue for entire site, if interval needs usage on an element, then use RenderQue.add(..., ...) */
relax.tools.renderQue = {
	items: [],
	running: false,
	rAF: null,
	// interval: 0,

	render: function()
	{
		if(!this.items || this.items.length === 0 || !this.running)
		{
			this.running = false;
			cancelAnimationFrame(this.rAF);
			return;
		}

		for (var i = 0; i < this.items.length; i++) {
			this.items[i].renderQueCall();
		}

		if(this.running)
			this.rAF = window.requestAnimationFrame(relax.bind(this, this.render));
	},
	has: function(elem)
	{
		return this.items.indexOf(elem) != -1;
	},
	add: function(elem)
	{
		if(elem && this.items)
		{
			if(this.items.indexOf(elem) === -1)
				this.items.push(elem);
		}

		if(!this.running)
		{
			this.running = true;
			this.rAF = window.requestAnimationFrame(relax.bind(this, this.render));
		}
	},
	remove: function(elem)
	{
		if(this.items && elem)
		{
			if(this.items.indexOf(elem) !== -1)
				this.items.splice(this.items.indexOf(elem), 1);
		}
		
		if(this.items.length === 0)
			this.running = false;
	}
};


/* clones JS objects */
relax.tools.clone = function(src)
{
    function mixin(dest, source, copyFunc)
    {
        var name, s, i, empty = {};
        for(name in source)
        {
            // the (!(name in empty) || empty[name] !== s) condition avoids copying properties in "source"
            // inherited from Object.prototype.	 For example, if dest has a custom toString() method,
            // don't overwrite it with the toString() method that source inherited from Object.prototype
            s = source[name];
            if(!(name in dest) || (dest[name] !== s && (!(name in empty) || empty[name] !== s)))
            {
                dest[name] = copyFunc ? copyFunc(s) : s;
            }
        }
        return dest;
    }

    if(!src || typeof src != "object" || Object.prototype.toString.call(src) === "[object Function]")
    {
        // null, undefined, any non-object, or function
        return src;	// anything
    }
    if(src.nodeType && "cloneNode" in src)
    {
        // DOM Node
        return src.cloneNode(true); // Node
    }
    if(src instanceof Date)
    {
        // Date
        return new Date(src.getTime());	// Date
    }
    if(src instanceof RegExp)
    {
        // RegExp
        return new RegExp(src);   // RegExp
    }
    var r, i, l;
    if(src instanceof Array)
    {
        // array
        r = [];
        for(i = 0, l = src.length;i < l;++i)
        {
            if(i in src)
            {
                r.push(clone(src[i]));
            }
        }
        // we don't clone functions for performance reasons
        //		}else if(d.isFunction(src)){
        //			// function
        //			r = function(){ return src.apply(this, arguments); };
    } else
    {
        // generic objects
        r = src.constructor ? new src.constructor() : {};
    }
    return mixin(r, src, clone);

};

relax.setLog = function(ignore) {
	if(ignore)
	{
		window.log = function(){};
	}else
	{
		//set log
		if(relax.browser.getInternetExplorerVersion() === -1)
		{
			//paul irish, IE not happy about this one.
			Function.prototype.bind&&(typeof console=="object"||typeof console=="function")&&typeof console.log=="object"&&["log","info","warn","error","assert","dir","clear","profile","profileEnd"].forEach(function(method){console[method]=this.call(console[method],console)},Function.prototype.bind);window.log||(window.log=function(){var ua,winRegexp,script,i,args=arguments,isReallyIE8=!1,isReallyIE8Plus=!1;log.history=log.history||[];log.history.push(arguments);if(log.detailPrint&&log.needDetailPrint){ua=navigator.userAgent;winRegexp=/Windows\sNT\s(\d+\.\d+)/;console&&console.log&&/MSIE\s(\d+)/.test(ua)&&winRegexp.test(ua)&&parseFloat(winRegexp.exec(ua)[1])>=6.1&&(isReallyIE8Plus=!0)}if(isReallyIE8Plus||typeof console!="undefined"&&typeof console.log=="function")if(log.detailPrint&&log.needDetailPrint&&log.needDetailPrint()){console.log("-----------------");args=log.detailPrint(args);i=0;while(i<args.length){console.log(args[i]);i++}}else Array.prototype.slice.call(args).length===1&&typeof Array.prototype.slice.call(args)[0]=="string"?console.log(Array.prototype.slice.call(args).toString()):console.log(Array.prototype.slice.call(args));else if(!Function.prototype.bind&&typeof console!="undefined"&&typeof console.log=="object")if(log.detailPrint){Function.prototype.call.call(console.log,console,Array.prototype.slice.call(["-----------------"]));args=log.detailPrint(args);i=0;while(i<args.length){Function.prototype.call.call(console.log,console,Array.prototype.slice.call([args[i]]));i++}}else Function.prototype.call.call(console.log,console,Array.prototype.slice.call(args));else if(!document.getElementById("firebug-lite")){script=document.createElement("script");script.type="text/javascript";script.id="firebug-lite";script.src="https://getfirebug.com/firebug-lite.js";document.getElementsByTagName("HEAD")[0].appendChild(script);setTimeout(function(){window.log.apply(window,args)},2e3)}else setTimeout(function(){window.log.apply(window,args)},500)});
		}else
		{
			window.log = function f(){ log.history = log.history || []; log.history.push(arguments); if(this.console) { var args = arguments, newarr; args.callee = args.callee.caller; newarr = [].slice.call(args); if (typeof console.log === 'object') log.apply.call(console.log, console, newarr); else console.log.apply(console, newarr);}};
		}
	}
};





// // shim layer for requestAnimationFrame with setTimeout fallback
(function() {
	var lastTime = 0;
	var vendors = ['ms', 'moz', 'webkit', 'o'];
	for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelRequestAnimationFrame = window[vendors[x]+
			'CancelRequestAnimationFrame'];
	}

	if (!window.requestAnimationFrame)
		window.requestAnimationFrame = function(callback, element) {
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id = window.setTimeout(function() { callback(currTime + timeToCall); },
				timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};

	if (!window.cancelAnimationFrame)
		window.cancelAnimationFrame = function(id) {clearTimeout(id);};
}());
